#-------------------------------------------------
#
# Project created by QtCreator 2016-08-03T05:04:32
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VacumChamber
TEMPLATE = app


SOURCES += main.cpp\
        firstwindow.cpp \
    mainwindow.cpp \
    qcustomplot.cpp

HEADERS  += firstwindow.h \
    mainwindow.h \
    qcustomplot.h
