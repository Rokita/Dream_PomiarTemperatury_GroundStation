#include "firstwindow.h"
#include <QtGui>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

FirstWindow::FirstWindow(QWidget *parent) : QDialog(parent)
{
    serial = new QSerialPort;
    bFindPorts = new QPushButton(tr("Find Ports"));
    connect(bFindPorts, SIGNAL(released()),
            this, SLOT(findports()));
    lPortName = new QLabel(tr("Port Name:"));
    cbPortName = new QComboBox;
    connect(cbPortName, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbPortNameChanged(QString)));
    lBaudRate = new QLabel(tr("Baud Rate:"));
    setComboBoxBaudRate();
    lDataBits = new QLabel(tr("Data Bits:"));
    setComboBoxDataBits();
    lParity   = new QLabel(tr("Parity:"   ));
    setComboBoxParity();
    lStopBits = new QLabel(tr("Stop Bits:"));
    setComboBoxStopBits();
    lFlowControl = new QLabel("Flow Control:");
    setComboBoxFlowControl();
    bConnect = new QPushButton(tr("Connect"));
    bConnect->setEnabled(false);
    bCancel = new QPushButton(tr("Exit"));
    bCancel->setDefault(true);
    connect(bCancel, SIGNAL(clicked()),
            this, SLOT(close()));
    connect(bConnect, SIGNAL(clicked()),
            this, SLOT(afterClick()));
    setMainLayout();
    setWindowTitle(tr("Dream Project - Serial Port Settings"));
}

void FirstWindow::afterClick()
{  
    this->setVisible(false);
    emit connecting(*serial);
}

void FirstWindow::findports()
{
    while(cbPortName->count()) //usun wszystkie aktualne porty
        cbPortName->removeItem(cbPortName->currentIndex());
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) //dla kazdego znalezionego portu (urzadzenia) wykonaj
        cbPortName->addItem(info.portName());
    if(cbPortName->count()) //jesli sa jakies porty to przycisk polaczenia aktywny
        bConnect->setEnabled(true);
    else                    //jesli nie ma portow to przycisk nieaktywny
        bConnect->setEnabled(false);
}

void FirstWindow::setComboBoxBaudRate()
{
    cbBaudRate = new QComboBox;
    connect(cbBaudRate, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbBaudRateChanged(QString)));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud1200));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud2400));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud4800));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud9600));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud19200));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud38400));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud57600));
    cbBaudRate->addItem(QString::number(QSerialPort::Baud115200));
    cbBaudRate->setCurrentIndex(3);
}

void FirstWindow::setComboBoxDataBits()
{
    cbDataBits = new QComboBox;
    connect(cbDataBits, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbDataBitsChanged(QString)));
    cbDataBits->addItem(QString::number(QSerialPort::Data5));
    cbDataBits->addItem(QString::number(QSerialPort::Data6));
    cbDataBits->addItem(QString::number(QSerialPort::Data7));
    cbDataBits->addItem(QString::number(QSerialPort::Data8));
    cbDataBits->setCurrentIndex(3);
}

void FirstWindow::setComboBoxParity()
{
    cbParity = new QComboBox;
    connect(cbParity, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbParityChanged(QString)));
    cbParity->addItem("No Parity");
    cbParity->addItem("Even Parity");
    cbParity->addItem("Odd Parity");
    cbParity->addItem("Space Parity");
    cbParity->addItem("Mark Parity");
    cbParity->setCurrentIndex(0);
}

void FirstWindow::setComboBoxStopBits()
{
    cbStopBits = new QComboBox;
    connect(cbStopBits, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbStopBitsChanged(QString)));
    cbStopBits->addItem(QString::number(QSerialPort::OneStop));
    cbStopBits->addItem(QString::number(QVariant(QSerialPort::OneAndHalfStop).toFloat()/2));
    cbStopBits->addItem(QString::number(QSerialPort::TwoStop));
}

void FirstWindow::setComboBoxFlowControl()
{
    cbFlowControl = new QComboBox;
    connect(cbFlowControl, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(cbFlowControlChanged(QString)));
    cbFlowControl->addItem("No Flow Control");
    cbFlowControl->addItem("Hardware Control");
    cbFlowControl->addItem("Software Control");
    cbFlowControl->setCurrentIndex(0);
}

void FirstWindow::setMainLayout()
{
    QHBoxLayout *buttons = new QHBoxLayout;
    buttons->addWidget(bCancel);
    buttons->addWidget(bConnect);
    QHBoxLayout *portName = new QHBoxLayout;
    portName->addWidget(lPortName);
    portName->addWidget(cbPortName);
    QHBoxLayout *baudRate = new QHBoxLayout;
    baudRate->addWidget(lBaudRate);
    baudRate->addWidget(cbBaudRate);
    QHBoxLayout *dataBits = new QHBoxLayout;
    dataBits->addWidget(lDataBits);
    dataBits->addWidget(cbDataBits);
    QHBoxLayout *parity = new QHBoxLayout;
    parity->addWidget(lParity);
    parity->addWidget(cbParity);
    QHBoxLayout *stopBits = new QHBoxLayout;
    stopBits->addWidget(lStopBits);
    stopBits->addWidget(cbStopBits);
    QHBoxLayout *flowControl = new QHBoxLayout;
    flowControl->addWidget(lFlowControl);
    flowControl->addWidget(cbFlowControl);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(bFindPorts);
    layout->addLayout(portName);
    layout->addLayout(baudRate);
    layout->addLayout(dataBits);
    layout->addLayout(parity);
    layout->addLayout(stopBits);
    layout->addLayout(flowControl);
    layout->addLayout(buttons);
    setLayout(layout);
}

void FirstWindow::cbPortNameChanged(const QString &str)
{
    serial->setPortName(str);
}

void FirstWindow::cbBaudRateChanged(const QString &str)
{
    serial->setBaudRate(str.toInt());
}

void FirstWindow::cbDataBitsChanged(const QString &str)
{
    serial->setDataBits((QSerialPort::DataBits)str.toInt());
}

void FirstWindow::cbParityChanged(const QString &str)
{
    if(str == "No Parity")
        serial->setParity(QSerialPort::NoParity);
    else if(str == "Even Parity")
        serial->setParity(QSerialPort::EvenParity);
    else if(str == "Odd Parity")
        serial->setParity(QSerialPort::OddParity);
    else if(str == "Space Parity")
        serial->setParity(QSerialPort::SpaceParity);
    else if(str == "Mark Parity")
        serial->setParity(QSerialPort::MarkParity);
}

void FirstWindow::cbStopBitsChanged(const QString &str)
{
    if(str=="1.5")
        serial->setStopBits(QSerialPort::OneAndHalfStop);
    else
        serial->setStopBits((QSerialPort::StopBits)str.toLong());
}

void FirstWindow::cbFlowControlChanged(const QString &str)
{
    if(str == "No Flow Control")
        serial->setFlowControl(QSerialPort::NoFlowControl);
    else if(str == "Hardware Control")
        serial->setFlowControl(QSerialPort::HardwareControl);
    else if(str == "Software Control")
        serial->setFlowControl(QSerialPort::SoftwareControl);
}

void FirstWindow::portNotAvailable()
{
    this->setVisible(true);
}
