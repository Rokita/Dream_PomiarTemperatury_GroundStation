/*!
  * \file
  * \brief Definicja klasy MainWindow
  *
  * Plik zawiera definicję klasy MainWindow będącej pochodną klasy QDialog i zawiera definicję wszystkich metod sygnałów i slotów wykorzystanych do budowy tej klasy
  *
*/

#include "mainwindow.h"
#include "qcustomplot.h"
#include <QtCore>
#include <QtGui>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QLabel>
#include <QTableWidget>
#include <QFile>
#include <QMessageBox>
#include <QLineEdit>
#include <QTextStream>


MainWindow::MainWindow()
   {
   resetBuffer();
   createAllObjects();
   setNewLayout();
   this->resize(1300, 700);

   serial = new QSerialPort;
   }
/*!
 * \brief MainWindow::~MainWindow Destruktor klasy MainWindow
 *
 * Metoda odpowiada za zamknięcie otwartych plików oraz zakończenie komunikacji z portem szeregowym
 */
MainWindow::~MainWindow()
   {
   qDebug() << "Destructor" <<endl;
   if(serial->isOpen())
      serial->close();
   if(mFile->isOpen())
      mFile->close();
   }

/*!
 * \brief MainWindow::connecting Slot klasy MainWindow. Wywoływany jest wyłącznie przez sygnał o tej samej nazwie z klasy FirstWindow.
 *
 * Metoda odpowiada za połączenie się głównego okna aplikacji z portem szeregowym wybranym w oknie FirstWindow
 *
 * \param FirstWindowSerial Obiekt klasy QSerialPort zawierający wszystkie pola swojej struktury uzupełnione przez metody klasy FirstWindow
 */
void MainWindow::connecting(const QSerialPort &FirstWindowSerial)
   {
   Qt::WindowFlags flags = Qt::Window | Qt::WindowSystemMenuHint |
                           Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint;
   this->setWindowFlags(flags);
   serial->setPortName(FirstWindowSerial.portName());
   serial->setBaudRate(FirstWindowSerial.baudRate());
   serial->setDataBits(FirstWindowSerial.dataBits());
   serial->setParity(FirstWindowSerial.parity());
   serial->setStopBits(FirstWindowSerial.stopBits());
   serial->setFlowControl(FirstWindowSerial.flowControl());
   if(!(serial->open(QIODevice::ReadWrite)))
      {
      QMessageBox msg;
      msg.setWindowTitle(tr("Error"));
      msg.setText(tr("Utracono połączenie z portem szeregowym. Czy zamknac aplikacje?"));
      msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

      if(msg.exec() == QMessageBox::No)
         emit portNotAvailable();
      else
         emit quitApplication();
      }
   else
      this->show();
   connect(serial, SIGNAL(readyRead()),
           this, SLOT(readData()));
   }

/*!
 * \brief MainWindow::readData Slot klasy MainWindow wywoływany przez sygnał readyRead() klasy QSerialPort.
 *
 * Odpowiada za odbiór danych z portu szeregowego i zapisanie tyvh danych do bufora
 */

QVector<uint8_t> vector;
void MainWindow::readData()
   {

   QByteArray pomiary = serial->readAll();
   qDebug()<<"Size: " << pomiary.size()<<endl;
   for(uint8_t iterator = 0; iterator<pomiary.size(); iterator++)
      {
      qDebug() << "Data"<<iterator << ": " << pomiary.at(iterator) <<endl;
      vector.push_back(pomiary.at(iterator));
      }
   qDebug()<<"ListaSize" <<vector.size();


   while(vector.size()>1 && vector.first()!=0xA5)
      {
      vector.pop_front();
      if(vector.first() == 0x5A)
         {
         vector.push_front(0xA5);
         break;
         }
      }


   while(vector.size() >= DATA_IN_FRAME)
      {

      for(uint8_t i=0; i<DATA_IN_FRAME; ++i)
         {
         buffer[i] = vector.first();
         vector.pop_front();
         }
      int sumaCRC=0;
      for(int i=LINEAR_TEMP; i<CRC1; i++)
         sumaCRC+=buffer[i];
      if(buffer[CRC1]==(sumaCRC%254)+1)
         this->useBuffer();
      }
   }
/*!
 * \brief MainWindow::useBuffer Metoda klasy MainWindow wywoływana ze Slotu readData().
 *
 * Po odebraniu dancyh z portu szeregowego przez slot readData metoda ta uzupełni odpowiednie pola tablicy danych pomiarowych oraz wypisze wartości na wykresach
 */
void MainWindow::useBuffer()
   {
   double bufor[DATA_IN_FRAME];
   for(uint8_t i=0; i<DATA_IN_FRAME; ++i)
      bufor[i]=(double)buffer[i];
   bufor[LINEAR_TEMP]=bufor[LINEAR_TEMP]-50;
   bufor[DRILLING_TEMP]=bufor[DRILLING_TEMP]-50;
   bufor[CAMERA_TEMP]=bufor[CAMERA_TEMP]-50;
   bufor[CLUTCH_TEMP]=bufor[CLUTCH_TEMP]-50;
   bufor[DCDC_TEMP]=bufor[DCDC_TEMP]-50;
   bufor[DRIVER_TEMP]=bufor[DRIVER_TEMP]-50;
   (*out)<<bufor[LINEAR_TEMP]  <<  "\t";
   (*out)<<bufor[DRILLING_TEMP]    <<  "\t";
   (*out)<<bufor[CAMERA_TEMP]    <<  "\t";
   (*out)<<bufor[CLUTCH_TEMP]   <<  "\t";
   (*out)<<bufor[DCDC_TEMP]   <<  "\t";
   (*out)<<bufor[DRIVER_TEMP]    <<  "\t";
   (*out)<<bufor[CRC1]    <<  "\r\n";

   //    if(bufor[MODE]==0x0F)
   //        value[0]->setText(QString("TEST_MODE"));
   //    else if(bufor[MODE]==0xF0)
   //   value[0]->setText(QString("FLIGHT_MODE"));
   //   value[1]->setText(QString("%1").arg(bufor[TEMP1]));
   //   value[2]->setText(QString("%1").arg(bufor[TEMP2]));
   //   value[3]->setText(QString("%1").arg(bufor[TMCU]));
   //   value[4]->setText(QString("%1").arg(bufor[PWM]));
   //   value[5]->setText(QString("%1").arg(bufor[CURRENT]));
   //   value[6]->setText(QString("%1").arg(bufor[VOLTAGE]));

   plotTemperature->graph(0)->addData(time, bufor[LINEAR_TEMP]);
   plotTemperature->graph(1)->addData(time, bufor[DRILLING_TEMP]);
   plotTemperature->graph(2)->addData(time, bufor[CAMERA_TEMP ]);
   plotTemperature->graph(3)->addData(time, bufor[CLUTCH_TEMP ]);
   plotTemperature->graph(4)->addData(time, bufor[DCDC_TEMP ]);
   plotTemperature->graph(5)->addData(time, bufor[DRIVER_TEMP ]);
   plotTemperature->xAxis->setRange(time+2, 4,Qt::AlignRight);
   plotTemperature->replot();


   time+=0.2;
   }

/*!
 * \brief MainWindow::resetBuffer Metoda klasy Mainwindow wywoływana z kilku miejsc (np. ze slotu connecting() )
 *
 * Odpowiada za wyzerowanie wszystkich wartości znajdujących się w buforze.
 */
void MainWindow::resetBuffer()
   {
   for(int i=0; i<DATA_IN_FRAME; ++i)
      buffer[i]=0;
   }

/*!
 * \brief MainWindow::switchMode Slot klasy MainWindow odpowiedzialny za zmianę trybu pracy systemu naziemnegoo
 *
 * Daje możliwość przełącznie się z tryb TEST_MODE w tryb FLIGHT_MODE i odwrotnie
 */
void MainWindow::switchMode()
   {
   if(mFile->isOpen())
      {
      QMessageBox msg;
      msg.setWindowTitle(tr("Question"));
      msg.setText(tr("Do you want to stop measurements"));
      msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
      if(msg.exec() == QMessageBox::Yes)
         {
         qDebug()<<"File is open";
         mFile->close();
         }
      }
   if(!mFile->isOpen())
      {
      if(bSwitchMode->text()=="Switch to FLIGHT Mode")
         {
         QMessageBox msg;
         msg.setWindowTitle(tr("Question"));
         msg.setText(tr("Do you want to have possibility to get back to Test_Mode?"));
         msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
         if(msg.exec() == QMessageBox::Yes)
            {

            bSwitchMode->setText("Switch to TEST Mode");
            lSelectCommand->setVisible(false);
            cbSelectCommand->setVisible(false);
            bSendCommand->setVisible(false);
            bAutomaticTest->setVisible(false);
            }
         else
            {
            bSwitchMode->setText("Switch to TEST Mode");
            bSwitchMode->setEnabled(false);
            lSelectCommand->setVisible(false);
            cbSelectCommand->setVisible(false);
            bSendCommand->setVisible(false);
            bAutomaticTest->setVisible(false);
            }
         }
      else
         {
         bSwitchMode->setText("Switch to FLIGHT Mode");
         lSelectCommand->setVisible(true);
         cbSelectCommand->setVisible(true);
         bSendCommand->setVisible(true);
         bAutomaticTest->setVisible(true);
         }
      bStartMeasurements->setEnabled(true);
      mFileName->setEnabled(true);
      measurementsAvailable = false;
      }
   }

/*!
 * \brief MainWindow::measurementStart Slot klasy MainWindow odpowiedzialny za reakcję na przycisk StartMeasurements
 *
 * Po kliknięciu na przycisk StartMeasurements metoda ta sprawdza, czy podana została odpowiednia nazwa pliku oraz czy jest on dostepny do otworzenia
 */
void MainWindow::measurementStart()
   {
   if(mFile->isOpen())
      {
      mFile->close();
      }
   }

/*!
 * \brief MainWindow::createAllObjects Jedna z najważniejszych metod klasy MainWindow
 *
 * Odpowiada za stworzenie (powołanie) wszystkich komponentów z których składa się główne okno aplikacji
 */
void MainWindow::createAllObjects()
   {
   plotTemperature = new QCustomPlot;
   plotTemperature->yAxis->setRange(-60, 160);
   plotTemperature->xAxis->setLabel("time");
   plotTemperature->yAxis->setLabel("Temperature [*C]");
   plotTemperature->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
   plotTemperature->addGraph();
   plotTemperature->graph(0)->setName("LINEAR_TEMP");
   plotTemperature->graph(0)->setPen(QPen(Qt::red));
   plotTemperature->addGraph();
   plotTemperature->graph(1)->setName("DRILLING_TEMP");
   plotTemperature->graph(1)->setPen(QPen(Qt::green));
   plotTemperature->addGraph();
   plotTemperature->graph(2)->setName("CAMERA_TEMP");
   plotTemperature->graph(2)->setPen(QPen(Qt::blue));
   plotTemperature->addGraph();
   plotTemperature->graph(3)->setName("CLUTCH_TEMP");
   plotTemperature->graph(3)->setPen(QPen(Qt::cyan));
   plotTemperature->addGraph();
   plotTemperature->graph(4)->setName("DCDC_TEMP");
   plotTemperature->graph(4)->setPen(QPen(Qt::magenta));
   plotTemperature->addGraph();
   plotTemperature->graph(5)->setName("DRIVER_TEMP");
   plotTemperature->graph(5)->setPen(QPen(Qt::yellow));
   plotTemperature->legend->setVisible(true);


   //   plotRPM         = new QCustomPlot;
   //   plotRPM->yAxis->setRange(-120, 120);
   //   plotRPM->xAxis->setLabel("time");
   //   plotRPM->yAxis->setLabel("DrillingPWM");
   //   plotRPM->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
   //   plotRPM->addGraph();
   //   plotRPM->graph(0)->setPen(QPen(Qt::red));

   //   plotVoltage     = new QCustomPlot;
   //   plotVoltage->yAxis->setRange(-5, 25);
   //   plotVoltage->xAxis->setLabel("time");
   //   plotVoltage->yAxis->setLabel("Voltage [V]");
   //   plotVoltage->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
   //   plotVoltage->addGraph();
   //   plotVoltage->graph(0)->setPen(QPen(Qt::red));

   //   plotCurrent     = new QCustomPlot;
   //   plotCurrent->yAxis->setRange(-6, 6);
   //   plotCurrent->xAxis->setLabel("time");
   //   plotCurrent->yAxis->setLabel("Current [A]");
   //   plotCurrent->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
   //   plotCurrent->addGraph();
   //   plotCurrent->graph(0)->setPen(QPen(Qt::red));

   bSwitchMode = new QPushButton("Switch to FLIGHT Mode");
   bSwitchMode->setMinimumHeight(50);
   closeButton = new QPushButton("CLOSE");
   connect(bSwitchMode, SIGNAL(released()),
           this, SLOT(switchMode()));
   lSelectCommand = new QLabel("Select Test Command: ");
   cbSelectCommand = new QComboBox;
   bSendCommand = new QPushButton("Send Command");
   bAutomaticTest = new QPushButton("Full automatic Test");
   cbSelectCommand->addItem("Command1");
   cbSelectCommand->addItem("Command2");
   cbSelectCommand->addItem("Command3");
   cbSelectCommand->addItem("Command4");
   cbSelectCommand->addItem("Command5");
   cbSelectCommand->addItem("Command6");
   tValueTable = new QTableWidget(7,2);
   param[0] = new QTableWidgetItem("Mode:");
   param[1] = new QTableWidgetItem("Motor Temp1:");
   param[2] = new QTableWidgetItem("Motor Temp2:");
   param[3] = new QTableWidgetItem("MCU Temp:");
   param[4] = new QTableWidgetItem("PWM:");
   param[5] = new QTableWidgetItem("Current:");
   param[6] = new QTableWidgetItem("Voltage:");
   tValueTable->setHorizontalHeaderLabels(QString("Parameter; Value").split(";"));
   for(uint8_t i=0; i<7; i++)
      tValueTable->setItem(i,0, param[i]);
   tValueTable->setColumnWidth(0,110);

   value[0] = new QTableWidgetItem(QString("%1").arg(0));
   value[1] = new QTableWidgetItem(QString("%1").arg(0));
   value[2] = new QTableWidgetItem(QString("%1").arg(0));
   value[3] = new QTableWidgetItem(QString("%1").arg(0));
   value[4] = new QTableWidgetItem(QString("%1").arg(0));
   value[5] = new QTableWidgetItem(QString("%1").arg(0));
   value[6] = new QTableWidgetItem(QString("%1").arg(0));
   for(uint8_t i=0; i<7; i++)
      tValueTable->setItem(i,1, value[i]);
   bStartMeasurements = new QPushButton("Stop Measurements");
   connect(bStartMeasurements, SIGNAL(released()),
           this, SLOT(measurementStart()));
   connect(closeButton, SIGNAL(released()),
           this, SLOT(slotClose()));
   mFile = new QFile("plik.txt");
   mFile->open(QIODevice::WriteOnly | QIODevice::Text);
   lFileName = new QLabel("Save Data in file:");

   mFileName = new QLineEdit;
   out = new QTextStream(mFile);

   (*out)<<"LinearTemp:\tDrillingTemp:\tCameraTemp:\tClutchTemp:\tDcdcTemp:\tDriverTemp:\tCRC:\r\n";

   }



void MainWindow::slotClose()
   {
   if(mFile->isOpen())
      {
      mFile->close();
      }
   }



/*!
 * \brief MainWindow::setNewLayout Metoda klasy MainWindow odpowiedzialna za ukłąd wszystkich komponentów w głównym oknie aplikacji
 *
 * Metoda ta rozstawia wszystkie komponenty w taki sposób aby były skalowalne podczas zmniejszania i zwiększania okna.
 */
void MainWindow::setNewLayout()
   {
   //lewa strona
   QGridLayout *leftLayout = new QGridLayout;
   leftLayout->addWidget(plotTemperature  ,  1  ,  1  ,  3  ,  4  );
   //   leftLayout->addWidget(plotCurrent      ,  4  ,  1  ,  3  ,  4  );
   //   leftLayout->addWidget(plotRPM          ,  1  ,  5  ,  3  ,  4  );
   //   leftLayout->addWidget(plotVoltage      ,  4  ,  5  ,  3  ,  4  );
   //prawastrona
   QGridLayout *rightLayout = new QGridLayout;
   rightLayout->addWidget(bSwitchMode       , 1, 1, 1, 2);
   rightLayout->addWidget(lSelectCommand    , 2, 1, 1, 1);
   rightLayout->addWidget(cbSelectCommand   , 3, 2, 1, 1);
   rightLayout->addWidget(bSendCommand      , 4, 1, 2, 2);
   rightLayout->addWidget(bAutomaticTest    , 5, 1, 1, 2);
   rightLayout->addWidget(tValueTable       , 8, 1, 6, 2);
   rightLayout->addWidget(lFileName, 15,1,1,1);
   rightLayout->addWidget(mFileName, 16,1,1,2);
   rightLayout->addWidget(bStartMeasurements, 17,1, 2, 2);
   rightLayout->addWidget(closeButton , 18,1,2,2);
   //cale okno
   QGridLayout *layout = new QGridLayout;
   layout->addLayout(leftLayout    , 1, 1, 4, 4);
   layout->addLayout(rightLayout   , 1, 5, 4, 1);


   this->setLayout(layout);
   }
