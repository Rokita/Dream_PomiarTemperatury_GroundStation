#include "firstwindow.h"
#include "mainwindow.h"
#include <QApplication>
#include <QSerialPort>

int main(int argc, char *argv[])
{
  QApplication *a = new QApplication(argc, argv);
  FirstWindow *first = new FirstWindow;
  MainWindow *window = new MainWindow;     //Okno które się otworzy po kliknieciu w przycisk connect
  QObject::connect(first, SIGNAL(connecting(QSerialPort)),
                   window, SLOT(connecting(QSerialPort)));
  QObject::connect(window, SIGNAL(portNotAvailable()),
                   first, SLOT(portNotAvailable()));
  QObject::connect(window, SIGNAL(finished(int)),
                   a, SLOT(quit()));
  QObject::connect(first, SIGNAL(finished(int)),
                   a, SLOT(quit()));
  first->show();
  first->resize(400,300);
  return a->exec();
}
