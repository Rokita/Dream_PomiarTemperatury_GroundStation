/*!
  *\file
  * \brief Plik zawierający definicje klasy MainWindow
  *
  * W pliku można znaleźć definicje klasy MainWindow oraz jej atrybutów, metod, sygnałów i slotów
  *
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QDialog>

#define SYNCHRO1    0xA5
#define SYNCHRO2    0x5A



class QLayout;
class QSerialPort;
class QCustomPlot;
class QLabel;
class QComboBox;
class QPushButton;
class QTableWidget;
class QFile;
class QLineEdit;
class QTextStream;
class QTableWidgetItem;

/*!
 * \brief Zawiera zdefiniowany format ramki odbieranych danych z portu szeregowego.
 */
enum frame{
   SYNC1=0       ,
   SYNC2         ,
   LINEAR_TEMP        ,
   DRILLING_TEMP        ,
   CAMERA_TEMP          ,
   CLUTCH_TEMP         ,
   DCDC_TEMP         ,
   DRIVER_TEMP         ,
   CRC1          ,
   DATA_IN_FRAME
   };
/*!
 * \brief Opisuje główne okno aplikacji.
 *
 * Zawiera definicję wszystkich komponentów wykorzystanych w głównym oknie aplikacji,
 * określa ich położenie oraz zachowanie w przypadku wystąpienia różnych sygnałów.
 * Obiekt klasy MainWindow powstaje po porawnej próbie połączenia z portem szeregowym.
 */

class MainWindow : public QDialog
   {
   Q_OBJECT
public:
   /*!
  * \brief Inicjalizacja głównego okna aplikacji.
  *
  * Pozainicjalizacją okna dopasowywuje layout (format) okna oraz wywołuje wszystkie
  * niezbędne metody potrzebne do wystartowania aplikacji takie jak np: createAllObjects().
  * Dodatkowo w konstruktorze tworzony jest obiekt portu szeregowego wykorzystywany
  * przy komunikacji z systemem kontrolno-pomiarowym eksperymentu DREAM.
 */
   MainWindow();
   /*!
    * \brief Zwalnia przydzieloną dla głównego okna aplikkacji pamięć oraz zamyka pootwierane pliki.
  */
   ~MainWindow();
private:
   /*!
   * \brief Czyści bufor odebranych danych.
   *
   * Po każdorazowym odebraniu i przetworzeniu ramki danych bufor jest zerowany z wykorzystaniem tej metody.
   */
   void resetBuffer();
   /*!
   * \brief Tworzy (powołuje do zycia) wszystkie komponenty z których ma się składać główne okno aplikacji.
   *
   * Po stworzeniu i zainicjowaniu komponentów składowych klasy MainWindow wywoływana jest metoda setNewLayout()
   * ustawiająca komponenty w odpowiednich miejscach okna aplikacji.
   */
   void createAllObjects();
   /*!
   * \brief definiuje położenie widgetów w oknie aplikacji.
   *
   * Ustawia stworzone przez metodę createAllObjects() komponenty w oknie aplikacji z wykorzystaniem klasy QLayout.
   *
   */
   void setNewLayout();
   /*!
   * \brief Przetwarzanie danych pobranych z portu szeregowego.
   *
   * Odebrane dane zostają przetworzone na wartości pomiarowe i wyświetlone na wykresach oraz w tablicy aktualnie odebranych wartości.
   */
   void useBuffer();

private slots:
   void slotClose();

   /*!
   * \brief Wywoływany w celu nawiązania połączenia z portem szeregowym.
   *
   * Przychodzący parametr musi zawierać wypełnione pola struktury odpowiadającej parametrom wywołania połączenia z portem szeregowym.
   */
   void connecting(const QSerialPort &);
   /*!
   * \brief Wywoływany w przypadku odebrania serii danych z portu szeregowego.
   *
   * Zczytuje z portu szeregowego przychodzace w postaci ramek dane, przelicza je na wartości liczbowe i zapisuje w pamięci.
   */
   void readData();
   /*!
   * \brief Zmienia tryb pracy.
   *
   * Zmienia tryb pracy systemu z TestMode na FlightMode i odwrotnie zarówno w eksperymencie jak i w systemie naziemnym.
   */
   void switchMode();
   /*!
   * \brief Rozpoczyna zapis danych do pliku.
   *
   * Wywoływany w celu rozpoczęcia zapisywania danych pomiarowych do pliku.
   * Wymaga wypełnionego pola z nazwą pliku, oraz kliknięcia przycisku startMeasurements.
   */
   void measurementStart();
signals:
   void signalClose();
   /*!
   * \brief Wywoływane w celu zakończenia pracy aplikacji.
   *
   * Równolegle wywoływane są destruktory obiektów, zwalniane są przydzielone zasoby pamięci,
   * zamykane sa wszystkie otwarte pliki oraz kończy się działanie aplikacji.
   */
   void quitApplication();
   /*!
   * \brief Generowny gdy nie można się połączyć z portem szeregowym.
   *
   * Powoduje wyświetlenie komunikatu o problemach dotyczących nawiązywania
   * połączenia z portem szeregowym i daje użytkownikowi możliwość wyboru,
   * czy zakmnąć aplikację, czy przejść do okna konfiguracyjnego połączenie.
   */
   void portNotAvailable();
private:
   /*!
   * \brief Flaga określająca czy dane są zapisywane w pliku czy nie.
   *
   * Wykorzystywany jako flaga do sprawdzania czy kiedyś wcześniej wciśnięto przycisk
   * StartMeasurement i rozpoczęto zapis danych pomiarowych do pliku
   */
   bool measurementsAvailable=false; //flaga - jeśli przyszła ramka i wciśnieto wczesniej przycisk start measurements
   /*!
   * \brief Zawiera aktualnie odebrane dane.
   *
   * Dane odebrane z portu szeregowego są rzutowane na 8 bitowy typ bez znaku i
   * przechowywane w buforze w celu dalszej obróbki odebranych danych.
   */
   uint8_t buffer[DATA_IN_FRAME];
   /*!
   * \brief Przechowuje wirtualny czas pracy systemu.
   *
   * Wykorzystywany do przechowywania sztucznie wygenerowanego czasu używanego
   * w celu przedstawienia wartości pomiarowych na wykresach
   * (Dane generowane przez system są cykliczne i o stałym okresie).
   */
   double time=0;

   /*!
   * \brief Obiekt wykorzystywany w celu nawiązania komunikacji z portem szeregowym.
   *
   * Wykorzystywany do połączenia się z portem szeregowym i komunikacji z systemem pomiarowym.
   * Użytkownik poprzez wybór odpowiednich parametrów połączenia w oknie konfiguracyjnym określił
   * z którym portem i w jaki sposób ma zostać nawiązane połączenie.
   */
   QSerialPort *serial;
   /*!
   * \brief Wykres przedstawiający zmiany temperatury zanotowane w czasie pracy systemu kontrolno-pomiarowego.
   *
   * Wykorzystywany do wizualizacji temperatury podzespołów znajdujących się w eksperymencie projektu DREAM.
   * Odbierane z portu szeregowego dane, po przetworzeniu są wykorzystywane do wygenerowania wykresu.
   */
   QCustomPlot *plotTemperature;

   /*!
   * \brief Odpowiada za zmianę trybu pracy aplikacji oraz systemu kontrolno-pomiarowego.
   *
   * Wykorzystywany do zmiany trybu pracy systemu GroundStation oraz eksperymentu z trybu TEST_MODE na FLIGHT_MODE oraz odwrotnie.
   * Zmiana ta musi zostać zatwierdzona przez system kontrolno-pomiarowy poprzez odesłanie aktualnego trybu pracy.
   */
   QPushButton *bSwitchMode;
   /*!
   * \brief Okresla miejsce wyboru komendy do wysłania.
   *
   * Określa napis znajdujący się przy wyborze komendy. Dostępny jest wyłącznie w trybie TEST_MODE.
   */
   QLabel *lSelectCommand;
   /*!
   * \brief Zawiera spis liste komend możliwych do wysłania z systemu GroundStation do systemu kontrolno-pomiarowego.
   *
   */
   QComboBox *cbSelectCommand;
   /*!
   * \brief Powoduje wysłanie wybranej komendy do systemu kontrolno-pomiarowego.
   *
   */
   QPushButton *bSendCommand;
   /*!
   * \brief Powoduje wysłanie do systemu okntrolno-pomiarowego komendy rozpoczynającej automatyczny test wszystkich podzespołów.
   *
   */
   QPushButton *bAutomaticTest;
   /*!
   * \brief Zawiera najaktualniejsze wartości mierzone przez system kontrolno-pomiarowy.
   *
   * Wartości przechowywane w tej tabelce są aktualizowane z każdą odebraną z portu szeregowego ramką.
   *
   */
   QTableWidget *tValueTable;
   /*!
   * \brief Składnik wykorzystywany w tablicy zawierajacej mierzone parametry.
   *
   */
   QTableWidgetItem *param[7];
   /*!
   * \brief Składnik wykorzystywany w tablicy zawierającej mierzone parametry.
   */
   QTableWidgetItem *value[7];
   /*!
   * \brief Określa uchwyt do pliky w którym są zapisywane wszystkie mierzone wartości.
   */
   QFile *mFile;


   QPushButton *closeButton;
   /*!
   * \brief Powoduje rozpoczęcie zapisywania mierzonych wartości do pliku.
   *
   * Rozpoczęscie zapisywania danych do pliku jest możliwe wyłącznie po wpisaniu poprawnej nazwy pliku w polu mFileName.
   */
   QPushButton *bStartMeasurements;
   /*!
   * \brief Wykorzystywany informacyjne aby użytkownik wiedział do czego służy pole edycyjne poniżej.
   *
   */
   QLabel *lFileName;
   /*!
   * \brief Pole edycyjne gdzie użytkownik musi wpisac nazwę pliku w którym mają zostać zapisywane mierzone wartości.
   *
   */
   QLineEdit *mFileName;
   /*!
   * \brief Wykorzystywany do łatwego wpisywania parametrów do pliku tekstowego.
   */
   QTextStream *out;
   };

#endif // MAINWINDOW_H
