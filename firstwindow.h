/*!
  * \file
  * \brief Plik zawierający definicję klasy FirstWindow
  *
  * Zawiera definicję klasy FirstWindow jest atrybutów, metod sygnałów i slotów.
  * Klasa ta jest wykorzystywana do stworzenia okna ukazującego się zaraz po uruchomieniu programu.
  *
*/
#ifndef FIRSTWINDOW_H
#define FIRSTWINDOW_H
#include <QDialog>

class QLabel;
class QPushButton;
class QComboBox;
class QSerialPort;
class QString;

/*!
 * \brief Definiuje wygląd pierwszego okna aplikacji.
 *
 * Klasa określa wygląd i funkcjonalność początkowego okna aplikacji,
 * w którym użytkownik wybiera parametry połączenia z portem szeregowym.
 */
class FirstWindow : public QDialog
{
  Q_OBJECT
public:
    /*!
   * \brief Inicjalizuje pierwsze okno aplikacji i określa położenie i wygląd komponentów .
   *
   * Konstruktor ten inicjalizuje wygląd pierwszego okna aplikacji, który ukazuje się zaraz po uruchomieniu programu.
   * Definiuje położenie komponentów, ich wygląd oraz połączenia między sygnałami i slotami.
   *
   * \param parent - parametr zawierający wskaźnik na 'rodzica' danej klasy.
   */
  FirstWindow(QWidget *parent=0);
  /*!
   * \brief Objekt służący do nawiązania komunikacji z portem szeregowym
   *
   * Atrybut odpowiedzialny za komunikację z portem szeregowym, Użytkownik poprzez wybór odpowiednich opcji w oknie konfiguracyjnym portu szeregowego,
   * uzupełnia pola struktury tego obiektu, które służą później (w klasie MainWindow do nawiązania połączenia z dostępnym portem szeregowym)
   */
  QSerialPort *serial;
private:
  /*!
   * \brief Uzuełnia listę dostepnych wartości parametru BaudRate.
   *
   * Metoda wywoływana w celu ustawienia listy wartości parametru Baudrate wykorzystywanej przy komunikacji szeregowej.
   * Parametr wybrany z pośród ustawionych w tej funkcji określa częstotliwość z jaką przesyłane są dane z systemu
   * kontrolno-pomiarowego poprzez port szeregowy do stacji naziemnej.
   */
  void setComboBoxBaudRate();
  /*!
   * \brief Uzupełnia listę dostępnych wartości parametru DataBits.
   *
   * Metoda wywoływana w celu ustawienia listy wartości parametru Databits wykorzystywanej przy komunikacji szeregowej.
   * Parametr wybrany z pośród ustawionych w tej funkcji określa ilość bitów danych występujących w ramce jednej danej przesyłanej
   * poprzez port szeregowy.
   */
  void setComboBoxDataBits();
  /*!
   * \brief Uzupełnia listę dostępnych wartości parametru Parity.
   *
   * Metoda wywoływana w celu ustawienia listy wartości parametru Parity wykorzystywanej przy komunikacji szeregowej.
   * Parametr ten określa czy występuje w ramce jednej danej bit parzystości czy nie, i jeśli występuje to co on określa (parzystość, nieparzystość).
   */
  void setComboBoxParity();
  /*!
   * \brief Uzupełnia listę dostępnych wartości prametru StopBits wykorzystywanych przy komunikacji szeregowej.
   *
   * Metoda wywoływana w celu ustawienia listy wartości parametru StopBits wykorzystywanej podczas komunikacji szeregowej.
   * Parametr ten określa ilość bitów stopu jakie mają występować na końcu każdej przesyłanej wartości 8-bitowej.
   *
   */
  void setComboBoxStopBits();
  /*!
   * \brief Uzupełnia listę dostępnych wartości prametru FlowControl wykorzystywanego przy komunikacji szeregowej.
   *
   * Metoda wywoływana w celu ustawienia listy wartości parametru FlowControl wykorzystywanego podczaskomunikacji szeregowej.
   * Parametr ten określa ilość bitów stopu jakie mają występować na końcu każdej przesyłanej wartości 8-bitowej.
   *
   */
  void setComboBoxFlowControl();
  /*!
   * \brief Ustawia wszystkie stworzone komponenty w zadany sposób.
   *
   * Metoda odpowiedzialna za ułożenie komponentów na pierwszym oknie aplikacji, wywoływana jest na początku uruchamiania aplikacji.
   */
  void setMainLayout();
private slots:
  /*!
   * \brief Wywoływny w momencie gdy nie można się połączyć z portem szeregowym.
   *
   * W efekcie jego wywołania pojawia się okienko z wiadomością dotyczącą problemu występującego w danej chwili w aplikacji.
   * Użytkownik może wtedy zamknąć aplikację bądź przejść spowrotem do do okna konfigurującego komunikację z portem szeregowym.
   */
  void portNotAvailable();
  /*!
   * \brief Wywoływany w przypadku kliknięcia na przycisk "Connect"
   *
   * Powoduje dezaktywawanie widoczności przycisku Connect oraz uruchamia główne okno aplikacji przesyłając mu argument
   * zawierający niezbędne dane do otworzenia portu.
   */
  void afterClick();
  /*!
   * \brief Odpowiada za wyszukanie dostępnych portów szeregowych
   *
   * Wywoływany po kliknięciu przycisku FIND PORTS, przeszukuje wszystkie dostępne i otwarte porty z którymi może się skomunikować.
   *
   */
  void findports();
  /*!
   * \brief Wywoływany w celu zmiany dostępnego portu
   *
   * Wywoływany przy zmianie wybranego portu w opcjach konfiguracyjnych.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbPortNameChanged(const QString &);
  /*!
   * \brief Wywoływany w celu zmiany parametru BaudRate
   *
   * Wywoływany przy zmianie wybranej wartości tego parametru w opcjach konfiguracyjnych.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbBaudRateChanged(const QString &);
  /*!
   * \brief Wywoływany w celu zmiany parametru DataBits
   *
   * Wywoływany w opcjach konfiguracyjnych przy zmianie ilości wysyłanych bajtów stopu.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbDataBitsChanged(const QString &);
  /*!
   * \brief Wywoływany w celu zmiany parametru Parity
   *
   * Wywoływany w opcjach konfiguracyjnych przy wyborze wartości bitu parzystości w przesyłanych ramkach danych.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbParityChanged(const QString &);
  /*!
   * \brief Wywoływany w celu zmiany parametru StopBits
   *
   * Wywoływany w opcjach konfiguracyjnych przy wyborze ilości bitów stopu w przesyłanych ramkach danych.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbStopBitsChanged(const QString &);
  /*!
   * \brief Wywoływany w celu zmiany parametru Flow Control
   *
   * Wywoływany w opcjach konfiguracyjnych przy wyborze sposobu wspierania przesyłu danych.
   * Aktualizuje on także wartości i parametry w strukturze przesyłanej do klasy MainWindow, czyli głównego okna aplikacji.
   */
  void cbFlowControlChanged(const QString &);
signals:
  /*!
   * \brief Generowany w celu wystartowania głównego okna aplikacji.
   *
   * Generowny po kliknięciu przycisku Connect po sprawdzeniu poprawności danych struktury wysyłanej do klasy MainWindow,
   * w celu umożliwienia wystartowania głownego okna aplikacji.
   */
  void connecting(const QSerialPort&);
private:
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru PortName.
   *
   * Napis określający miejsce gdzie wybiera się port komunikacji szeregowej.
   */
  QLabel *lPortName;      //napis dotyczacy okna portname
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru BaudRate.
   *
   *  Napis poprzedzający miejsce wyboru parametru baudRate wykorzystywanego w komunikacji szeregowej.
   */
  QLabel *lBaudRate;      //napis dotyczący czestotliwosci wysylanych danych
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru DataBits.
   *
   * Napis poprzedzający miejsce wyboru parametru DataBits.
   */
  QLabel *lDataBits;      //napis dotyczacy ilosci bitow w ramce danych
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru Parity.
   *
   * Napis poprzedzający miejsce wyboru parametru Parity wykorzystywanego w komunikacji szeregowej.
   */
  QLabel *lParity;        //napis dotyczacy bitu parzystosci w ramce danych
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru StopBits.
   *
   * Napis poprzedzający miejsce wyboru parametru StopBits wykorzystywanego w komunikacji szeregowej.
   */
  QLabel *lStopBits;      //napis dotyczacy ilosci bitow stop w ramce danych
  /*!
   * \brief Pole tekstowe informujące o miejscu ustawiania parametru FlowControl.
   *
   * Napis poprzedzający miejsce wyboru parametru FlowControl wykorzystywanego w komunikacji szeregowej.
   */
  QLabel *lFlowControl;    //napis dotyczacy kontroli przeplywu danych
  /****************/
  /*!
   * \brief Lista dostępnych portów komunikacji szeregowej.
   *
   * Miejsce wyboru jednego z dostępych portów szeregowych. Aktualizacja dostępnych portów następuje po kliknięciu na przycisk FindPorts.
   */
  QComboBox *cbPortName;  //wybor portu
  /*!
   * \brief Lista dostępnych wartości parametru baudrate.
   *
   * Miejsce wyboru prędkości przeysłania danych poprzez port szeregowych.
   * Do wyboru są wyłącznie prędkości standardowe zdefiniowane w strukturze klasy QSerialPort.
   */
  QComboBox *cbBaudRate;  //wybor czestotliowsci
  /*!
   * \brief Lista dostępnych wartości parametru DataBits.
   *
   * Miejsce wyboru ilości bitów danych podczas przeysłania danych poprzez port szeregowych.
   * Do wyboru są wyłącznie standardowe długości danych zdefiniowane w strukturze klasy QSerialPort.
   */
  QComboBox *cbDataBits;  //wybor ilosci bitow
  /*!
   * \brief Lista dostępnych wartości parametru Parity.
   *
   * Miejsce wyboru obecności oraz typu parzystości podczas przeysłania danych poprzez port szeregowych.
   * Do wyboru są wyłącznie typy parzystości zdefiniowane w strukturze klasy QSerialPort.
   */
  QComboBox *cbParity;    //akceptacja parzystosci
  /*!
   * \brief Lista dostępnych wartości parametru StopBits.
   *
   * Miejsce wyboru ilości bitów stopu podczas przeysłania danych poprzez port szeregowych.
   * Do wyboru są wyłącznie standardowe ilości bitów stopu zdefiniowane w strukturze klasy QSerialPort.
   */
  QComboBox *cbStopBits;  //wybor ilosci bitow stopu
  /*!
   * \brief Lista dostępnych wartości parametru FlowControl.
   *
   * Miejsce wyboru wsparcia przeysłania danych poprzez port szeregowych.
   * Do wyboru są wyłącznie standardowe rodzaje takiego wsparcia zdefiniowane w strukturze klasy QSerialPort.
   */
  QComboBox *cbFlowControl; //wybranie kontroli przeplywu
  /**********************/
  /*!
   * \brief Przycisk odpowiadający próbie połączenia z portem szeregowym.
   *
   * Po naciśnięciu na przycisk sprawdzana jest próba skomunikowania się z wybranym portem szeregowym.
   * Jeśli wszystko przebiegło bezbłędnie to wywoływane jest główne okno aplikacji.
   * Domyślnie przycisk nieaktywny. Zostaje aktywowany tylko w przypadku znalezienia jakiegoś dostępnego portu szeregowego.
   *
   */
  QPushButton *bConnect;  //przycisk do polaczenia z odpowiednim portem
  /*!
   * \brief Przycisk rezygnujący z korzystania z aplikacji.
   *
   * Przycisk dostępy dla użytkownika i cały czas aktywny powodujący zamknięcie wszystkich otwartych okien,
   * zwolnienie przydzielonej pamięci oraz wyłączenia aplikacji. Wszystkie ustawione parametry zostają utracone,
   * a po ponowym uruchomieniu aplikacji wymagane będzie ponowne ich ustawianie.
   */
  QPushButton *bCancel;   //przycisk do anulowania - bedzie wylaczal program
  /*!
   * \brief Przycisk odpowiedzialny za wyszukiwanie w systemie dostępnych portów komunikacji szeregowej.
   *
   * Odpowiada za wyszukiwanie dostępnych do połączenia portów szeregowych.
   * Wyłącznie z jego pomocą możliwe jest aktywowanie przycisku Connect, pod warunkiem znalezienia jakiegokolwiek dostępnego portu.
   */
  QPushButton *bFindPorts; //przycisk sprawdza dostępność portów
};

#endif // FIRSTWINDOW_H
